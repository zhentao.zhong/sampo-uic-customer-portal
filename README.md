# homecredit-customer-portal

## Getting Started

Install dependencies

```bash
$ yarn install
```

Start the dev server

```bash
$ yarn start
```