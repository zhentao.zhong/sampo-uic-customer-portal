declare module '*.css';
declare module '*.less';
declare module '*.png';
declare module '*.svg' {
  export function ReactComponent(
    props: React.SVGProps<SVGSVGElement>,
  ): React.ReactElement;
  const url: string;
  export default url;
}

declare const ENV: string;
declare const BASE_URL: string;
declare const FILE_URL: string;
declare const PLATFORM: string;
declare const LOGIN_URL: string;

type ObjectAny = Record<string, any>;
