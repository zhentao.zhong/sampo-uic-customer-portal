import { defineConfig } from 'umi';

const FILE_URL = 'https://api.staging.iglooinsure.com/v1/branch/fileapi';
const BASE_URL = 'https://api.staging.iglooinsure.com/v1/home-credit';
const LOGIN_URL = 'https://api.staging.iglooinsure.com/v1/customer';

export default defineConfig({
  devtool: 'source-map',
  define: {
    ENV: 'staging',
    FILE_URL,
    BASE_URL,
    LOGIN_URL,
  },
});
