import { defineConfig } from 'umi';

const FILE_URL = 'https://api.dev.iglooinsure.com/v1/branch/fileapi';
const BASE_URL = 'https://api.dev.iglooinsure.com/v1/home-credit';
const LOGIN_URL = 'https://api.dev.iglooinsure.com/v1/customer';

export default defineConfig({
  devtool: 'source-map',
  define: {
    ENV: 'dev',
    FILE_URL,
    BASE_URL,
    LOGIN_URL,
  },
});
