module.exports = {
  extends: [require.resolve('@umijs/fabric/dist/eslint')],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      tsx: true,
    },
    project: './tsconfig.json',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  plugins: ['react-hooks'],
  rules: {
    'no-plusplus': 0,
    'no-restricted-syntax': 0,
    'no-shadow': 1,
    '@typescript-eslint/no-shadow': 0,
  },
};
