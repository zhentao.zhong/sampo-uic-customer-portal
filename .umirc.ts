import path from 'path';
import theme from 'iglootheme';
import { defineConfig } from 'umi';

const PLATFORM = 'SampoUIC';

export default defineConfig({
  title: PLATFORM,
  base: '/',
  publicPath: `/${PLATFORM}/`,
  outputPath: `/build/${PLATFORM}`,
  favicon: 'https://static.insurance.axinan.com/images/email/gip/favicon.ico',
  hash: true,
  nodeModulesTransform: {
    type: 'none',
  },
  alias: {
    '@/*': path.resolve(__dirname, 'src/*'),
    '@@/*': path.resolve(__dirname, 'src/.umi/*'),
  },
  define: {
    PLATFORM,
    FILE_URL: 'http://localhost:8000',
    BASE_URL: 'http://localhost:8000',
  },
  plugins: [
    './plugins/clean.js',
    './plugins/moveFiles.js',
    // './plugins/google-analytics.js',
  ],
  extraBabelPlugins: [],
  theme: {
    ...theme,
  },
  dynamicImport: {
    loading: '@/components/PageLoading',
  },
  chainWebpack: (memo) => {
    memo.module.rule('font').test(/\.otf/).use('babel').loader('url-loader');
  },
  locale: {},
  dva: {
    hmr: true,
    immer: true,
  },
});
