import { message } from 'iglooform';
import { cloneDeep } from 'lodash';
import type { RequestConfig } from 'umi';
import { decamelizeKeys, camelizeKeys } from 'humps';

import { getJWT, removeJWT } from '@/utils/jwt';
import { goUrl } from '@/utils/route';

/**
 * Decamelize Request Params Keys
 * @param url
 * @param options
 */
const decamelizeParams = (url: string, options: any) => {
  const { params, data, headers } = options;
  if (url.includes(BASE_URL) || url.includes(LOGIN_URL))
    return {
      url,
      options: {
        ...options,
        headers: {
          'X-Axinan-Authorization': getJWT(),
          ...headers,
        },
        params: decamelizeKeys(params),
        data: decamelizeKeys(data),
      },
    };
  return { url, options };
};

/**
 * Camelize Response Data Keys
 * @param response
 */
const camelizeData = async (response: any, options: any) => {
  const { status } = response;
  const { method } = options;
  const json = await response.json();

  const body = camelizeKeys(json, (key: string, convert) => {
    return /^[A-Z].+$/.test(key) ? key : convert(key);
  });

  switch (status) {
    case 200:
      return cloneDeep(body);
    default:
      return {
        method,
        responseStatus: status,
        ...json,
      };
  }
};

export const request: RequestConfig | { headers: Headers } = {
  prefix: BASE_URL,
  errorConfig: {
    adaptor,
  },
  requestInterceptors: [decamelizeParams],
  responseInterceptors: [camelizeData],
};

function adaptor(resData: any) {
  const { responseStatus } = resData;

  if (responseStatus) {
    message.error(resData.message);
  }
  // token expired, logout
  if (responseStatus === 401) {
    removeJWT();
    goUrl('/login');
  }

  return resData;
}
