import type { Reducer } from 'umi';

export type PromiseFunc = (...rest: any[]) => Promise<any>;

export interface ModelType {
  state: any;
  reducers: {
    setData: Reducer;
  };
}
