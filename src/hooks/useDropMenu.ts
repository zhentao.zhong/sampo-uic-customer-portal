import { useState, useEffect, useCallback } from 'react';

import { getLocalStorage, removeJWT } from '@/utils/jwt';

import { goUrl } from '@/utils/route';

const checkPath = (path: any[], id: string): boolean => {
  // eslint-disable-next-line no-restricted-syntax
  for (const dom of path) {
    if (dom.id === id) {
      return true;
    }
  }
  return false;
};

export default () => {
  const [toggle, setToggle] = useState(false);

  const [userInfo] = useState(() => {
    const userName = getLocalStorage('customerName');
    const userPhone = getLocalStorage('customerMobile');
    const userEmail = getLocalStorage('customerEmail');
    return {
      userName,
      userPhone,
      userEmail,
    };
  });

  const closeToggle = useCallback((e) => {
    if (!checkPath(e.path || e?.composedPath(), 'dropMenu')) {
      setToggle(false);
    }
  }, []);

  useEffect(() => {
    if (toggle) {
      document
        .getElementById('root')
        ?.addEventListener('click', closeToggle, { once: true });
    }
  }, [toggle]);

  const onToggle = (e) => {
    e.nativeEvent.stopImmediatePropagation();
    e.stopPropagation();
    setToggle((prevToggle) => {
      return !prevToggle;
    });
  };

  const onLogout = () => {
    removeJWT();
    goUrl('/login');
  };

  return {
    toggle,
    userInfo,
    onToggle,
    onLogout,
  };
};
