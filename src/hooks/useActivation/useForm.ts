import { history } from 'umi';
import { Form } from 'iglooform';
import { useRequest } from 'igloo-pa-tools';

import { createActivation, createDeathActivation } from '@/services/activation';
import { goUrl } from '@/utils/route';

import usePerson from './usePerson';
import useClaimant from './useClaimant';
import useBenefit from './useBenefit';
import useReimbursement from './useReimbursement';
import useReview from './useReview';
import { fullNameConvert } from './utils';

import type { IQuery } from '@/pages/h5/activation-death';

function useForm(isDeath: boolean, dataPrefill: any) {
  const { policyId } = (history.location.query || {}) as IQuery;

  const { doRequest, loading } = useRequest({
    func: isDeath ? createDeathActivation : createActivation,
  });

  const form = Form.useForm();

  const { personDetail } = usePerson(form, dataPrefill);
  const { claimantDetail } = useClaimant(form, isDeath);
  const { benefitDetail } = useBenefit(isDeath, dataPrefill);
  const { reimbursementDetail } = useReimbursement();
  const { review } = useReview();

  const config = {
    type: 'Steps',
    elements: [
      personDetail,
      claimantDetail,
      benefitDetail,
      reimbursementDetail,
      review,
    ],
  };

  const onSubmit = async (value: any): Promise<void> => {
    const { insured, claimant, benefit, reimbursement } = value;

    const relationshipTypeIsSelf = claimant?.relationshipType === 'Self';

    const payload = {
      createClaimRequest: {
        claimForm: {
          benefitsClaimed: {
            claimDocuments: {
              deathCertificate: benefit?.deathCertificate,
              hospitalCertificate: benefit?.hospitalCertificate,
              hospitalCertificateEr: benefit?.hospitalCertificateEr,
              hospitalSoa: benefit?.hospitalSoa,
              hospitalSoaEr: benefit?.hospitalSoaEr,
            },
            claimReason: benefit?.claimReason,
          },
          claimantDetail: {
            customerInfo: relationshipTypeIsSelf
              ? undefined
              : {
                  address: claimant?.address,
                  email: claimant?.email,
                  first: claimant?.firstName,
                  middle: claimant?.middleName,
                  last: claimant?.lastName,
                  fullName: fullNameConvert(
                    claimant?.firstName,
                    claimant?.middleName,
                    claimant?.lastName,
                  ),
                  identificationNumber: claimant?.identificationNumber,
                  identificationType: claimant?.identificationType,
                  mobile: claimant?.mobile?.phoneNumber,
                },
            relationshipCertificate: claimant?.relationshipCertificate,
            relationshipType: claimant?.relationshipType,
          },
          insuredDetail: {
            address: insured?.address,
            birth: dataPrefill?.birth, // can't edit，save the value from backend
            email: insured?.email,
            first: insured?.firstName,
            middle: insured?.middleName,
            last: insured?.lastName,
            fullName: fullNameConvert(
              insured?.firstName,
              insured?.middleName,
              insured?.lastName,
            ),
            gender: insured?.gender,
            identificationNumber: insured?.identificationNumber,
            identificationType: insured?.identificationType,
            mobile: insured?.mobile,
          },
          reimbursementDetail: {
            accountName: reimbursement?.accountName,
            accountNumber: reimbursement?.accountNumber?.phoneNumber,
            bankName: reimbursement?.bankName,
          },
        },
        policyId,
      },
    };

    const res = await doRequest(payload);
    if (res.claimId) {
      goUrl(
        `/${isDeath ? 'activation-death' : 'activation'}/confirmation?claimId=${
          res.claimId
        }`,
      );
    }
  };

  return {
    config,
    onSubmit,
    form,
    loading,
  };
}

export default useForm;
