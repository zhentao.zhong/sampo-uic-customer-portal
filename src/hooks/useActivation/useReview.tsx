import { Typography } from 'iglooform';

import TermModal from '@/components/TermModal';

import useDeclaration from '../useDeclaration';
import styles from './index.less';

function useForm() {
  const level = 'body2';

  const online = [
    "I agree to share my/insured's data with Igloo and its insurance partner to be collected and processed for insurance claim purposes.",
    'I declare that all the statements made in this claim form are accurate, true, and complete.',
    'I understand that making false, misleading, or incomplete statements may cause: \n(i) the claim to be rejected; \n(ii) insurance coverage to be rescinded by the Insurer; or \n(iii) legal action.',
  ];

  const { declarationDom, termsChecked } = useDeclaration({
    clickText: 'Declaration Terms',
    modal: (
      <TermModal title="Declaration Terms">
        <div>
          {online.map((declaration, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <div className={styles.listItem} key={index}>
              <Typography level={level} className={styles.number}>
                {index + 1}.{' '}
              </Typography>
              <Typography level={level}>{declaration}</Typography>
            </div>
          ))}
        </div>
      </TermModal>
    ),
  });

  const review = {
    type: 'Step',
    name: 'Review and Declaration',
    label: 'Review and Declaration',
    editButtonLabel: 'Edit',
    getButtonDisabledState: () => {
      if (termsChecked === true) {
        return false;
      }
      return true;
    },
    elements: [
      {
        render: () => {
          return (
            <>
              <Typography level="h3b">
                Please take a moment to review what you’ve told us above.
              </Typography>
              <Typography level="body1" className={styles.tips}>
                Once you’re happy with the above, and you’ve checked the box
                below, please go ahead and submit your claim.
              </Typography>
            </>
          );
        },
      },
      {
        type: 'Divider',
      },
      {
        render: () => {
          return <>{declarationDom}</>;
        },
      },
    ],
  };

  return {
    review,
  };
}

export default useForm;
