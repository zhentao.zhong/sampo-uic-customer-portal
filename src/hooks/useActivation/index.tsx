import { Form } from 'iglooform';
import { useRequest } from 'igloo-pa-tools';
import { history } from 'umi';

import { prefill } from '@/services/activation';

import useForm from './useForm';

import type { IQuery } from '@/pages/h5/activation-death';

const useActivation = (props: { isDeath: boolean }) => {
  const { isDeath } = props;

  const { policyId } = (history.location.query || {}) as IQuery;

  const { data: dataPrefill, loading: loadingPrefill } = useRequest({
    func: prefill,
    params: policyId,
    dealResponse: (res) => res.prefillInfo,
    defaultData: {},
    automatic: !!policyId,
  });

  const { config, onSubmit, form, loading } = useForm(isDeath, dataPrefill);

  const { policyDetail = {} } = dataPrefill;

  const messages = {
    validateMessages: {
      types: {
        email: 'Email Address is not a valid email',
      },
      required: 'This field is required',
      submitText: 'Submit',
      continueText: 'Continue',
    },
  };

  const getPrefillCompleted = Object.keys(dataPrefill).length > 0;

  const fromDom = getPrefillCompleted ? (
    <Form config={config} onSubmit={onSubmit} locales={messages} form={form} />
  ) : null;

  return {
    fromDom,
    loading,
    // prefill
    policyDetail,
    loadingPrefill,
  };
};

export default useActivation;
