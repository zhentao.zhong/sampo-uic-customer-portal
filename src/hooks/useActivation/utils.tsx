import { message } from 'antd';
import { checkPc } from 'igloo-pa-tools';

import { upload } from '@/services/service';
import {
  DAILY_HOSPITAL_VALUE,
  EMERGENCY_ROOM_VALUE,
  ACCIDENTAL_DEATH_VALUE,
  NATURAL_SICKNESS_DEATH_VALUE,
} from '@/params';

export const FORMATE = 'MM / DD / YYYY';

export const LIMIT_UPLOAD_NUM = 5;

export const UPLOAD_PLACEHOLDER = checkPc()
  ? undefined
  : 'Select a file to upload';

export const genderOpts = [
  {
    label: 'Female',
    value: 'Female',
  },
  {
    label: 'Male',
    value: 'Male',
  },
];

export const identificationTypeOpts = [
  {
    label: 'TIN',
    value: 'tin',
  },
  {
    label: 'Passport',
    value: 'passport',
  },
  {
    label: 'UMID',
    value: 'umid',
  },
  {
    label: 'SSS',
    value: 'sss',
  },
  {
    label: "Driver's License",
    value: 'drivers_license',
  },
  {
    label: 'National ID',
    value: 'national_id',
  },
  {
    label: 'PSA Birth Certificate',
    value: 'psa_birth_certificate',
  },
  {
    label: "Voter's ID",
    value: 'voters_id',
  },
  {
    label: 'Senior Citizen Card',
    value: 'senior_citizen_card',
  },
  {
    label: 'PRC ID',
    value: 'prc_id',
  },
];

export const relationshipOpts = [
  {
    label: 'Self',
    value: 'Self',
  },
  {
    label: 'Spouse',
    value: 'Spouse',
  },
  {
    label: 'Parent',
    value: 'Parent',
  },
  {
    label: 'Child',
    value: 'Child',
  },
  {
    label: 'Others',
    value: 'Others',
  },
];

const commonReason = [
  {
    label: 'Daily Hospital Income Benefit (sickness and accident)',
    value: DAILY_HOSPITAL_VALUE,
  },
  {
    label: 'Emergency Room Assistance (sickness and accident)',
    value: EMERGENCY_ROOM_VALUE,
  },
];

const accidentDeathClaimReason = {
  label: 'Accidental Burial Benefit',
  value: ACCIDENTAL_DEATH_VALUE,
  disabled: true,
};

const sicknessDeathClaimReason = {
  label: 'Burial Cash Assistance due to Natural sickness',
  value: NATURAL_SICKNESS_DEATH_VALUE,
  disabled: true,
};

export const noDeathClaimReasonOpts = [...commonReason];

export const accidentDeathClaimReasonOpts = [
  accidentDeathClaimReason,
  ...commonReason,
];

export const sicknessDeathClaimReasonOpts = [
  sicknessDeathClaimReason,
  ...commonReason,
];

export const allClaimReason = [
  accidentDeathClaimReason,
  sicknessDeathClaimReason,
  ...commonReason,
];

export const claimReasonRule = [
  {
    validator: (rule: any, value: string) => {
      return new Promise<void>((resolve, reject) => {
        if (value.includes(EMERGENCY_ROOM_VALUE)) {
          reject(
            'the "Emergency Room Assistance (sickness and accident)" can only be filed once',
          );
        } else {
          resolve();
        }
      });
    },
  },
];

export const handleUpload = async (file: any) => {
  try {
    const { url } = await upload(file);
    if (url) return url;
    throw new Error('Upload Failed');
  } catch (err) {
    message.error('Upload Failed');
    return Promise.reject();
  }
};

export const fullNameConvert = (
  firstName: string,
  middleName: string | undefined,
  lastName: string,
) => {
  if (middleName) {
    return `${firstName} ${middleName} ${lastName}`;
  } else {
    return `${firstName} ${lastName}`;
  }
};
