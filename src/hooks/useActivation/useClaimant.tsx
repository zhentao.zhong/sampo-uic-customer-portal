import type { FormInstance } from 'antd';

import { mapValueToLabelText } from '@/utils';
import { ACCEPT } from '@/params';

import {
  relationshipOpts,
  identificationTypeOpts,
  handleUpload,
  LIMIT_UPLOAD_NUM,
  fullNameConvert,
  UPLOAD_PLACEHOLDER,
} from './utils';

const notSelfAsserts = [
  {
    field: ['claimant', 'relationshipType'],
    operation: 'ni',
    value: ['Self', undefined],
  },
];

function useClaimant(form: FormInstance, isDeath: boolean) {
  const claimantDetail = {
    type: 'Step',
    name: 'claimant',
    label: 'Claimant’s Details',
    editButtonLabel: 'Edit',
    elements: [
      {
        type: 'Select',
        label: 'Relationship to the Insured',
        name: 'relationshipType',
        halfRow: true,
        options: isDeath
          ? relationshipOpts.filter((item) => item.label !== 'Self')
          : relationshipOpts,
        previewFormater: (v: string) =>
          mapValueToLabelText(relationshipOpts, v),
      },
      isDeath && {
        type: 'Upload',
        label: 'Proof of Relationship to the Insured',
        name: 'relationshipCertificate',
        extra:
          'eg. Birth Certificate of Insured, Marriage Certificate, Baptisma (in the absence of Birth Certificate)',
        handleUpload,
        accept: ACCEPT,
        placeholder: UPLOAD_PLACEHOLDER,
        limit: LIMIT_UPLOAD_NUM,
        asserts: notSelfAsserts,
      },
      {
        type: 'Input',
        label: 'First Name',
        name: 'firstName',
        hideWhenPreview: true,
        asserts: notSelfAsserts,
      },
      {
        type: 'Input',
        label: 'Middle Name',
        name: 'middleName',
        showOptional: true,
        required: false,
        hideWhenPreview: true,
        asserts: notSelfAsserts,
      },
      {
        type: 'Input',
        label: 'Last Name',
        name: 'lastName',
        hideWhenPreview: true,
        halfRow: true,
        asserts: notSelfAsserts,
      },
      {
        type: 'Input',
        label: 'Full Name',
        name: 'lastName', // 绑定了edit中输入了值的name才能渲染出来
        hideWhenEdit: true,
        previewFormater: () => {
          const firstName = form.getFieldValue('claimant')?.firstName;
          const middleName = form.getFieldValue('claimant')?.middleName;
          const lastName = form.getFieldValue('claimant')?.lastName;
          return fullNameConvert(firstName, middleName, lastName);
        },
        asserts: notSelfAsserts,
      },
      {
        type: 'PhoneNumber',
        label: 'Mobile Number',
        name: 'mobile',
        areaCode: '',
        halfRow: true,
        asserts: notSelfAsserts,
      },
      {
        type: 'Email',
        label: 'Email',
        name: 'email',
        halfRow: true,
        asserts: notSelfAsserts,
      },
      {
        type: 'TextArea',
        label: 'Address',
        name: 'address',
        fullRow: true,
        asserts: notSelfAsserts,
      },
      {
        type: 'Select',
        label: 'Identification Type',
        name: 'identificationType',
        options: identificationTypeOpts,
        previewFormater: (v: string) =>
          mapValueToLabelText(identificationTypeOpts, v),
        asserts: notSelfAsserts,
      },
      {
        type: 'Input',
        label: 'Identification Number',
        name: 'identificationNumber',
        asserts: notSelfAsserts,
      },
    ].filter(Boolean),
  };

  return {
    claimantDetail,
  };
}

export default useClaimant;
