function useReimbursement() {
  const reimbursementDetail = {
    type: 'Step',
    name: 'reimbursement',
    label: 'Reimbursement Details',
    editButtonLabel: 'Edit',
    elements: [
      {
        type: 'Input',
        label: 'Account Name',
        name: 'accountName',
      },
      {
        type: 'PhoneNumber',
        label: 'Account Number',
        name: 'accountNumber',
        areaCode: '',
      },
      {
        type: 'Input',
        label: 'Bank Name',
        name: 'bankName',
        fullRow: true,
      },
    ],
  };

  return {
    reimbursementDetail,
  };
}

export default useReimbursement;
