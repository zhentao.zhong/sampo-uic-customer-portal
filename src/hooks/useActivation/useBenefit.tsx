import { history } from 'umi';
import { Typography } from 'iglooform';
import type { FormInstance } from 'antd';

import {
  ACCEPT,
  DAILY_HOSPITAL_VALUE,
  EMERGENCY_ROOM_VALUE,
  ACCIDENTAL_DEATH_VALUE,
  NATURAL_SICKNESS_DEATH_VALUE,
} from '@/params';

import {
  noDeathClaimReasonOpts,
  accidentDeathClaimReasonOpts,
  sicknessDeathClaimReasonOpts,
  handleUpload,
  claimReasonRule,
  LIMIT_UPLOAD_NUM,
  UPLOAD_PLACEHOLDER,
  allClaimReason,
} from './utils';

import styles from './index.less';

import type { IQuery } from '@/pages/h5/activation-death';

const optionsMap = {
  Accident: accidentDeathClaimReasonOpts,
  Sickness: sicknessDeathClaimReasonOpts,
};

const initialValueMap = {
  Accident: [ACCIDENTAL_DEATH_VALUE],
  Sickness: [NATURAL_SICKNESS_DEATH_VALUE],
};

function useBenefit(isDeath: boolean, dataPrefill: any) {
  const { deathType } = (history.location.query || {}) as IQuery;

  const { hasErClaim } = dataPrefill;

  const isNoDeath = !isDeath;

  const uploadConfig = {
    type: 'Upload',
    accept: ACCEPT,
    subscribedFields: ['benefit', 'claimReason'],
    limit: LIMIT_UPLOAD_NUM,
    placeholder: UPLOAD_PLACEHOLDER,
    handleUpload,
  };

  const benefitDetail = {
    type: 'Step',
    name: 'benefit',
    label: 'Benefits Claimed',
    editButtonLabel: 'Edit',
    elements: [
      {
        type: 'CheckboxGroup',
        label: <span className={styles.colorBlack}>Claim Reason</span>,
        name: 'claimReason',
        fullRow: true,
        options: isNoDeath ? noDeathClaimReasonOpts : optionsMap[deathType!],
        initialValue: isNoDeath ? [] : initialValueMap[deathType!],
        rules: hasErClaim ? claimReasonRule : undefined,
        previewFormater: (v: string) => {
          return allClaimReason
            .filter((item) => v.includes(item.value))
            .map((obj) => obj.label)
            .join('\n');
        },
      },
      {
        render: () => (
          <Typography level="body2" className={styles.uploadTip}>
            Only support PDF, JPG, JPEG or PNG file. Each file cannot exceed
            20MB.
          </Typography>
        ),
        label: <span className={styles.colorBlack}>Documents Upload</span>,
        hideWhenPreview: true,
        shouldRender: (form: FormInstance) => {
          const selectReason = form.getFieldValue('benefit')?.claimReason || [];
          return selectReason.length;
        },
        subscribedFields: ['benefit', 'claimReason'],
      },
      // death upload
      {
        label: 'Death Certificate',
        name: 'deathCertificate',
        extra: 'notarized copy',
        shouldRender: (form: FormInstance) => {
          const selectReason = form.getFieldValue('benefit')?.claimReason || [];
          return (
            selectReason.includes(ACCIDENTAL_DEATH_VALUE) ||
            selectReason.includes(NATURAL_SICKNESS_DEATH_VALUE)
          );
        },
        ...uploadConfig,
      },
      // daily hospital
      {
        label:
          'Hospital Certificate Bearing the Number of Days Confined at the Hospital',
        name: 'hospitalCertificate',
        extra:
          'Certificate being issued by Hospital in PH bearing their name and Hospital seal - Original',
        shouldRender: (form: FormInstance) => {
          const selectReason = form.getFieldValue('benefit')?.claimReason || [];
          return selectReason.includes(DAILY_HOSPITAL_VALUE);
        },
        ...uploadConfig,
      },
      {
        label: 'SOA from the Hospital',
        name: 'hospitalSoa',
        showOptional: true,
        required: false,
        extra: 'Statement of Account given by Hospitals to show the bills',
        shouldRender: (form: FormInstance) => {
          const selectReason = form.getFieldValue('benefit')?.claimReason || [];
          return selectReason.includes(DAILY_HOSPITAL_VALUE);
        },
        ...uploadConfig,
      },
      // emergency room
      {
        label: 'SOA from the Hospital on the use of ER',
        name: 'hospitalSoaEr',
        extra:
          'Statement with itemised billing that shows the used of Emergency Room facility of Hospital- Original',
        shouldRender: (form: FormInstance) => {
          const selectReason = form.getFieldValue('benefit')?.claimReason || [];
          return selectReason.includes(EMERGENCY_ROOM_VALUE);
        },
        ...uploadConfig,
      },
      {
        label: 'Hospital Certificate bearing the date at the emergency room',
        name: 'hospitalCertificateEr',
        extra:
          'Certificate being issued by Hospital in PH bearing their name and Hospital seal - Original',
        shouldRender: (form: FormInstance) => {
          const selectReason = form.getFieldValue('benefit')?.claimReason || [];
          return selectReason.includes(EMERGENCY_ROOM_VALUE);
        },
        ...uploadConfig,
      },
    ],
  };

  return {
    benefitDetail,
  };
}

export default useBenefit;
