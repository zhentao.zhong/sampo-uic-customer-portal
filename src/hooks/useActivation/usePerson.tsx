import type { FormInstance } from 'antd';
import moment from 'moment';

import { mapValueToLabelText } from '@/utils';

import { identificationTypeOpts, fullNameConvert } from './utils';

function useForm(form: FormInstance, dataPrefill: any) {
  const {
    address,
    birth,
    email,
    firstName,
    gender,
    lastName,
    middleName,
    mobile,
  } = dataPrefill;

  const personDetail = {
    type: 'Step',
    name: 'insured',
    label: 'Insured’s Details',
    editButtonLabel: 'Edit',
    elements: [
      {
        type: 'Input',
        label: 'First Name',
        name: 'firstName',
        hideWhenPreview: true,
        disabled: true,
        initialValue: firstName,
      },
      middleName && {
        type: 'Input',
        label: 'Middle Name',
        name: 'middleName',
        required: false,
        hideWhenPreview: true,
        disabled: true,
        initialValue: middleName,
      },
      {
        type: 'Input',
        label: 'Last Name',
        name: 'lastName',
        hideWhenPreview: true,
        disabled: true,
        halfRow: true,
        initialValue: lastName,
      },
      {
        type: 'Input',
        label: 'Full Name',
        name: 'lastName', // 绑定了edit中输入了值的name才能渲染出来
        hideWhenEdit: true,
        hidePreviewDivider: true,
        previewFormater: () => {
          const firstName = form.getFieldValue('insured')?.firstName;
          const middleName = form.getFieldValue('insured')?.middleName;
          const lastName = form.getFieldValue('insured')?.lastName;
          return fullNameConvert(firstName, middleName, lastName);
        },
      },
      {
        type: 'Input',
        label: 'Gender',
        name: 'gender',
        halfRow: true,
        disabled: true,
        initialValue: gender,
      },
      {
        type: 'InputDate',
        label: 'Date of Birth',
        name: 'birth',
        format: 'MM/DD/YYYY',
        disabled: true,
        halfRow: true,
        initialValue: moment(birth).format('MM/DD/YYYY'),
      },
      {
        type: 'Input',
        label: 'Mobile Number',
        name: 'mobile',
        halfRow: true,
        disabled: true,
        initialValue: mobile,
      },
      {
        type: 'Email',
        label: 'Email',
        name: 'email',
        initialValue: email,
        disabled: true,
      },
      {
        type: 'TextArea',
        label: 'Address',
        name: 'address',
        fullRow: true,
        disabled: true,
        initialValue: address,
      },
      {
        type: 'Select',
        label: 'Identification Type',
        name: 'identificationType',
        options: identificationTypeOpts,
        previewFormater: (v: string) =>
          mapValueToLabelText(identificationTypeOpts, v),
      },
      {
        type: 'Input',
        label: 'Identification Number',
        name: 'identificationNumber',
      },
    ].filter(Boolean),
  };

  return {
    personDetail,
  };
}

export default useForm;
