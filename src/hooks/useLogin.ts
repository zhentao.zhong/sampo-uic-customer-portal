import { useEffect } from 'react';
import { history } from 'umi';
import { useRequest } from 'igloo-pa-tools';

import { goUrl } from '@/utils/route';
import { getEmail, sendOtp, otpCheck } from '@/services/login';
import {
  setJWT,
  parseJWT,
  setLocalStorage,
  getLocalStorage,
  removeJWT,
} from '@/utils/jwt';

const optConfig = {
  source: 'HomeCredit',
  otpType: 'email_otp',
};

export default () => {
  const {
    location: { query = {} },
  } = history;

  const { doRequest, loading, data, error } = useRequest({ func: getEmail });

  // first come here get info by id
  useEffect(() => {
    removeJWT();
    // get id from localStorage,when the url don't have id
    if (!query.id || query.id === 'null') {
      const customerId = getLocalStorage('customerId');
      // go to 404 page,when the url and localStorage both don't have id
      if (!customerId || customerId === 'null') {
        history.push('/401');
      } else {
        goUrl(`/login?id=${customerId}`);
        doRequest(customerId);
      }
    } else {
      doRequest(query.id);
    }
  }, []);

  // get email error
  if (error) {
    history.push('/401');
  }

  const onSubmit = async (value: any) => {
    const res = await otpCheck({
      email: data?.email,
      otpCode: String(value.otp),
      ...optConfig,
    });
    if (!res.responseStatus) {
      const { email: customerEmail, full_name: customerName = '--' } = parseJWT(
        res.jwtToken,
      ) as {
        email: string;
        full_name: string;
      };

      setJWT(res.jwtToken);
      setLocalStorage({
        customerId: query.id as string,
        customerEmail,
        customerName,
      });
      goUrl('/policy');

      return undefined;
    }
    return [
      {
        name: 'otp',
        errors: ['OTP does not exist or has expired'],
      },
    ];
  };

  const onSend = async () => {
    await sendOtp({
      email: data?.email,
      ...optConfig,
    });
    return true;
  };

  return {
    onSubmit,
    email: data?.email,
    loading,
    onSend,
  };
};
