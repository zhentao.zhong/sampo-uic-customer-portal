import { useState } from 'react';

import BeforeStart from '@/components/BeforeStart';

const useBeforeStart = () => {
  const [{ policyId, visible }, setState] = useState<ObjectAny>({});

  const openModal = (policyId: string | undefined) => {
    setState({
      visible: true,
      policyId,
    });
  };

  const onCancel = () => {
    setState({
      visible: false,
      policyId: undefined,
    });
  };

  const renderModal = visible ? (
    <BeforeStart visible={visible} onCancel={onCancel} defaultId={policyId} />
  ) : null;

  return {
    openModal,
    renderModal,
  };
};

export default useBeforeStart;
