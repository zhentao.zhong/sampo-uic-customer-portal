import { useState } from 'react';
import { useRequest } from 'igloo-pa-tools';

import { cancelClaim } from '@/services/claim';
import CancelModal from '@/components/CancelModal';

const useCancel = () => {
  const [{ claimId, visible, callback }, setState] = useState<ObjectAny>({});

  const { loading, doRequest } = useRequest({
    func: cancelClaim,
  });

  const openModal = (claimId: string, callback: () => void) => {
    setState({
      claimId,
      visible: true,
      callback,
    });
  };

  const onCancel = () => {
    setState({
      claimId: undefined,
      visible: false,
      callback: () => {},
    });
  };

  const onOk = async () => {
    if (!loading) {
      const res = await doRequest(claimId);
      if (res?.ok === 'ok') {
        onCancel();
        callback();
      }
    }
  };

  const renderModal = (
    <CancelModal
      visible={visible}
      onCancel={onCancel}
      onOk={onOk}
      loading={loading}
    />
  );

  return {
    openModal,
    renderModal,
  };
};

export default useCancel;
