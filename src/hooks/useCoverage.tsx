import { useState } from 'react';
import { CoverageModal } from 'igloo-pa-tools';
import { Typography } from 'iglooform';

import { DAILY_HOSPITAL_VALUE, EMERGENCY_ROOM_VALUE } from '@/params';

import type { ReactNode } from 'react';

type Props = {
  coverageKeyMap: Record<string, any>;
  title?: ReactNode;
  cancelText?: ReactNode;
  prefix: string;
};

const usePreview = (props: Props) => {
  const {
    coverageKeyMap,
    title = 'Coverage',
    cancelText = 'Close',
    prefix,
  } = props;
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [coverageList, setCoverageList] = useState<any[]>([]);

  const closeModal = () => {
    setModalVisible(false);
    setCoverageList([]);
  };

  const openModal = (value: any[]) => {
    value.forEach((item) => {
      if (item.coverageKey === DAILY_HOSPITAL_VALUE) {
        item.after = (
          <>
            <Typography level="body1"> per day</Typography>
            <Typography level="caption2" style={{ color: '#999' }}>
              {' '}
              (max of 15 days)
            </Typography>
          </>
        );
      }
      if (item.coverageKey === EMERGENCY_ROOM_VALUE) {
        item.after = (
          <Typography level="caption2" style={{ color: '#999' }}>
            {' '}
            (one time use)
          </Typography>
        );
      }
    });

    setModalVisible(true);
    setCoverageList(value);
  };

  const renderModal = (
    <CoverageModal
      visible={modalVisible}
      title={title}
      onCancel={closeModal}
      cancelText={cancelText}
      okButtonProps={{ style: { display: 'none' } }}
      coverageKeyMap={coverageKeyMap}
      coverageList={coverageList}
      prefix={prefix}
    />
  );

  return {
    renderModal,
    openModal,
  };
};

export default usePreview;
