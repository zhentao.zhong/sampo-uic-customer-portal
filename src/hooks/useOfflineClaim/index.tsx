import { Typography } from 'iglooform';
import { checkPc } from 'igloo-pa-tools';

import useModal from '../useModal';

import styles from './index.less';

const InfoArea = (props: any) => {
  const { imgSrc, title, contentList } = props;
  const pcMark = checkPc();
  return (
    <div className={styles.infoAreaWrap}>
      <div>
        <img src={imgSrc} />
      </div>
      <div className={styles.infoAreaRight}>
        <div className={styles.infoAreaTitle}>
          <Typography level={pcMark ? 'h3a' : 'h5'} children={title} />
        </div>
        <ul className={styles.infoAreaContent}>
          {Array.isArray(contentList) &&
            contentList.map((item, idx) => {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <li key={idx}>
                  <Typography
                    level={pcMark ? 'body1' : 'body2'}
                    children={item}
                  />
                </li>
              );
            })}
        </ul>
      </div>
    </div>
  );
};

export default () => {
  const pcMark = checkPc();
  const OfflineClaim = (
    <>
      <InfoArea
        imgSrc="https://static.iglooinsure.com/icons/Notification.png"
        title="Our online claim service will be available soon. In the meantime,"
        contentList={[
          <>
            Please email us at{' '}
            <Typography
              level={pcMark ? 'h4' : 'h5'}
              children="medical.claims@mici.com.ph"
            />{' '}
            with your policy number and claim request
          </>,
          'Operating Hour: 9:00 AM - 4:00 PM Monday - Friday  (business hours)',
        ]}
      />
      <InfoArea
        imgSrc="https://static.iglooinsure.com/icons/File.png"
        title="Please prepare the required supporting documents as listed below in advance. Our customer support team will assist you on the claim procedure."
        contentList={[
          'Hospital Certificate Bearing the number of days confined at the hospital',
          'SOA from the hospital to show the bills',
          <>
            SOA from the hospital on the use of ER{' '}
            <span className={styles.stone}>
              (for Emergency Room Assistance claim)
            </span>
          </>,
        ]}
      />
    </>
  );
  const { renderModal, openModal } = useModal({
    children: OfflineClaim,
    modalProps: {
      title: 'Online claim is coming soon!',
      cancelText: 'OK',
      okButtonProps: { style: { display: 'none' } },
    },
  });

  return {
    renderModal,
    openModal,
  };
};
