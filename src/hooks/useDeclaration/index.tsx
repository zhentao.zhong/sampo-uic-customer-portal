import React, { useState } from 'react';
import { Checkbox } from 'antd';
import { Typography } from 'iglooform';

import styles from './index.less';

type Props = {
  clickText: any;
  modal: JSX.Element;
};

const useDeclaration = (props: Props) => {
  const [termsChecked, setTermsChecked] = useState(false);
  const [showTermsModal, setShowTermsModal] = useState(false);

  const { clickText, modal } = props;

  const handleOpenTermsModal = () => {
    setTermsChecked(!termsChecked);
  };

  const onOk = () => {
    setTermsChecked(true);
    setShowTermsModal(false);
  };

  const onCancel = () => {
    setShowTermsModal(false);
  };

  const declarationDom = (
    <>
      <div className={styles.checkbox}>
        <Checkbox onChange={handleOpenTermsModal} checked={termsChecked} />
        <div>
          <Typography level="body1">{'I agree to the '}</Typography>
          <span onClick={() => setShowTermsModal(true)}>
            <Typography level="h4" className={styles.blueColor}>
              {clickText}
            </Typography>
          </span>
          <Typography level="body1">{' of claim submission.'}</Typography>
        </div>
        {React.cloneElement(modal, {
          visible: showTermsModal,
          onCancel,
          onOk,
          footer: !termsChecked,
        })}
      </div>
    </>
  );

  return {
    declarationDom,
    termsChecked,
  };
};

export default useDeclaration;
