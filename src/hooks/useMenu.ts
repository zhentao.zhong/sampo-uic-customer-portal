import { useState, useEffect } from 'react';
import { history } from 'umi';

import { goUrl } from '@/utils/route';

const useMenu = () => {
  const pathname = history.location.pathname;
  const [activeMenu, setActiveMenu] = useState(pathname);

  const onChange = (url: string) => {
    goUrl(url);
    setActiveMenu(url);
  };

  useEffect(() => {
    setActiveMenu(pathname);
  }, [pathname]);

  return {
    onChange,
    activeMenu,
  };
};

export default useMenu;
