import { useEffect } from 'react';
import { PAGE_ROOT } from '@/params';

export default () => {
  useEffect(() => {
    document.getElementById(PAGE_ROOT)?.scrollTo(0, 0);
  }, []);
};
