import { useState } from 'react';
import { Modal } from 'igloo-pa-tools';

import ClaimAmount from '@/components/ClaimAmount';

const useApprovedAmount = () => {
  const [visible, setVisible] = useState(false);
  const [approvedInfo, setApprovedInfo] = useState<ObjectAny>({});

  const openModal = (obj: ObjectAny) => {
    setVisible(true);
    setApprovedInfo(obj);
  };

  const onCancel = () => {
    setVisible(false);
    setApprovedInfo({});
  };

  const renderModal = (
    <Modal
      title="Approved Claim Amount"
      visible={visible}
      onCancel={onCancel}
      cancelText="Close"
      okButtonProps={{ style: { display: 'none' } }}
    >
      <ClaimAmount approvedInfo={approvedInfo} />
    </Modal>
  );

  return {
    openModal,
    renderModal,
  };
};

export default useApprovedAmount;
