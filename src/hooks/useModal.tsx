import { useState } from 'react';
import { Modal } from 'igloo-pa-tools';

interface Props {
  children: string | React.ReactNode;
  modalProps: Record<string, any>;
}

const useModal = (props: Props) => {
  const { children, modalProps } = props;
  const [modalVisible, setModalVisible] = useState(false);

  const closeModal = () => {
    setModalVisible(false);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const renderModal = (
    <Modal visible={modalVisible} onCancel={closeModal} {...modalProps}>
      {children}
    </Modal>
  );

  return {
    renderModal,
    openModal,
    closeModal,
  };
};

export default useModal;
