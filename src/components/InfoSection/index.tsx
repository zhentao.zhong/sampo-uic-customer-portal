import type { ReactChild, FC } from 'react';
import { Typography } from 'iglooform';
import { Row, Col } from 'antd';

import styles from './index.less';

export interface InfoSectionProps {
  title?: ReactChild[] | ReactChild;
}

export interface InfoLayoutProps {
  title?: ReactChild | ReactChild[];
  span?: number;
  xs?: number;
  sm?: number;
  md?: number;
  className?: string;
}

export const InfoSection: FC<InfoSectionProps> = ({ title, children }) => {
  return (
    <div className={styles.infoBox}>
      <Typography level="h4" className={styles.title}>
        {title}
      </Typography>
      <Row gutter={32}>{children}</Row>
    </div>
  );
};

export const InfoLayout: FC<InfoLayoutProps> = ({
  title,
  children,
  span = 6,
  xs,
  sm,
  md,
  className,
}) => {
  return (
    <Col span={span} xs={xs} sm={sm} md={md} className={className}>
      <Typography className={styles.infoTitle} level="body1">
        {title}
      </Typography>
      <Typography level="body1">{children}</Typography>
    </Col>
  );
};
