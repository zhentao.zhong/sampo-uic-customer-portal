import { Typography, Form } from 'iglooform';
import { Loading } from 'igloo-pa-tools';

import { goUrl } from '@/utils/route';

import styles from './index.less';

interface LoginOptProps {
  onSubmit: (arg: any) => any | undefined;
  email: undefined | string;
  loading: boolean;
  onSend: () => Promise<any>;
}

export default ({ onSubmit, email, loading, onSend }: LoginOptProps) => {
  if (loading) {
    return <Loading />;
  }

  const gotoDeathFlow = () => {
    goUrl('/before-start');
  };

  return (
    <div className={styles.loginContent}>
      <Typography level="h2" className={styles.title}>
        Welcome to Igloo
      </Typography>
      <Typography level="body1" className={styles.tip}>
        Please use your email and OTP code to log in.
      </Typography>
      <Form
        locales={{
          submitText: 'Log In',
        }}
        config={{
          type: 'Login',
          elements: [
            {
              type: 'Email',
              label: 'Email',
              name: 'email',
              fullRow: true,
              initialValue: email,
              disabled: true,
            },
            {
              type: 'Otp',
              label: 'OTP Code',
              name: 'otp',
              sendOtp: onSend,
              countDownSeconds: 60,
              dependField: 'email',
              fullRow: true,
            },
          ],
        }}
        onSubmit={onSubmit}
      />
      <Typography level="body1" className={styles.deathTip}>
        For claims that include death benefits, please{' '}
        <span onClick={gotoDeathFlow}>click here</span> to complete and submit
        the claim form
      </Typography>
    </div>
  );
};
