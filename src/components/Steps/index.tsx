import { Steps } from 'igloo-pa-tools';
import { useIntl } from 'umi';

export default ({ current }: { current: number }) => {
  const { formatMessage } = useIntl();
  const STEP_LIST = [
    formatMessage({ id: 'Select Plan' }),
    formatMessage({ id: 'Activation' }),
    formatMessage({ id: 'Payment' }),
  ];
  return <Steps list={STEP_LIST} current={current} />;
};
