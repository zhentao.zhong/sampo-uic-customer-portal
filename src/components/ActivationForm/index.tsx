import { useIntl } from 'umi';
import { FormPage } from 'igloo-pa-tools';
import { HealthOutlined } from 'iglooicon';
import { Form, DetailPanel, Typography } from 'iglooform';

import { getRelationship, getGender, getIdentificationType } from '@/params';

import useReview from './elements/useReview';

import type { FC } from 'react';

import styles from './index.less';

const ActivationForm: FC = () => {
  const { formatMessage } = useIntl();

  const employDetails = {
    type: 'Step',
    name: 'employeeDetails',
    label: formatMessage({ id: 'Insured’s Details' }),
    editButtonLabel: formatMessage({ id: 'Edit' }),
    elements: [
      {
        type: 'PhoneNumber',
        areaCode: '+84',
        label: formatMessage({ id: 'Mobile Number' }),
        name: 'firstName',
        extra: formatMessage({
          id: 'Please make sure the mobile number is correct as it will be the customer account for policy and claims management',
        }),
      },
      {
        type: 'Input',
        name: 'fullName',
        label: formatMessage({ id: 'Full Name' }),
      },
      {
        type: 'Input',
        name: 'departmentName',
        label: formatMessage({ id: 'Department Name' }),
      },
      {
        type: 'InputNumber',
        name: 'employCode',
        label: formatMessage({ id: 'Employee Code' }),
      },
      {
        type: 'Email',
        name: 'email',
        label: formatMessage({ id: 'Email Address' }),
      },
    ],
  };

  const insureDetails = {
    type: 'Step',
    name: 'insuredDetails',
    label: formatMessage({ id: 'Insured’s Details' }),
    editButtonLabel: formatMessage({ id: 'Edit' }),
    elements: [
      {
        type: 'Select',
        name: 'relationship',
        label: formatMessage({ id: 'Relationship to the employee' }),
        options: getRelationship(),
      },
      {
        type: 'Input',
        name: 'fullName',
        label: formatMessage({ id: 'Full Name' }),
      },
      {
        type: 'InputDate',
        name: 'birth',
        format: 'DD/MM/YYYY',
        label: formatMessage({ id: 'Date of Birth' }),
      },
      {
        type: 'Select',
        name: 'gender',
        label: formatMessage({ id: 'Gender' }),
        options: getGender(),
      },
      {
        type: 'Select',
        name: 'idType',
        label: formatMessage({ id: 'Identification Type' }),
        options: getIdentificationType(),
      },
      {
        type: 'Input',
        name: 'idNo',
        label: formatMessage({ id: 'Identification Number' }),
      },
      {
        name: 'idNo',
        type: 'PhoneNumber',
        areaCode: '+84',
        label: formatMessage({ id: 'Mobile Number' }),
      },
      {
        type: 'Email',
        name: 'email',
        label: formatMessage({ id: 'Email Address' }),
      },
      {
        type: 'TextArea',
        name: 'residentialAddress',
        label: formatMessage({ id: 'Residential Address' }),
      },
    ],
  };

  const { review } = useReview();

  const config = {
    type: 'Steps',
    elements: [employDetails, insureDetails, review],
  };

  const onSubmit = () => {};

  return (
    <FormPage
      title={formatMessage({ id: 'Activate Plan' })}
      subtitle={formatMessage({
        id: 'All fields are required unless marked optional.',
      })}
      detailPanel={
        <DetailPanel
          title={formatMessage({ id: 'Plan Details' })}
          subTitle={formatMessage({ id: 'Hospital Cash Allowance' })}
          icon={<HealthOutlined />}
        >
          <DetailPanel.DetailItem
            title={formatMessage({ id: 'Selected Plan' })}
            value={
              <div className={styles.detailPanelLink}>
                <span>Plan 3</span>
                <div onClick={() => {}}>
                  <Typography level="h4">
                    {formatMessage({ id: 'Change' })}
                  </Typography>
                </div>
              </div>
            }
          />
          <DetailPanel.DetailItem
            title={formatMessage({ id: 'Billing Period' })}
            value="Monthly"
          />
          <DetailPanel.DetailItem
            title={formatMessage({ id: 'Including Maternity Allowance' })}
            value="Yes"
          />
          <DetailPanel.DetailItem
            title={formatMessage({ id: 'Premium' })}
            value={`VND ${51100}/month`}
          />
        </DetailPanel>
      }
      loading={false}
    >
      <Form
        config={config}
        onSubmit={onSubmit}
        locales={{
          validateMessages: {
            types: {
              email: formatMessage({
                id: 'Email Address is not a valid email',
              }),
            },
            required: formatMessage({ id: 'This field is required' }),
            submitText: formatMessage({ id: 'Submit' }),
            continueText: formatMessage({ id: 'Continue' }),
          },
        }}
      />
    </FormPage>
  );
};

export default ActivationForm;
