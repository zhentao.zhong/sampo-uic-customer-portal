import { Typography } from 'iglooform';
import { useIntl } from 'umi';

import TermModal from '@/components/TermModal';
import useDeclaration from '@/hooks/useDeclaration';

import styles from './index.less';

const useReview = () => {
  const { formatMessage } = useIntl();

  const online = [
    "I agree to share my/insured's data with Igloo and its insurance partner to be collected and processed for insurance claim purposes.",
    'I declare that all the statements made in this claim form are accurate, true, and complete.',
    'I understand that making false, misleading, or incomplete statements may cause: \n(i) the claim to be rejected; \n(ii) insurance coverage to be rescinded by the Insurer; or \n(iii) legal action.',
  ];

  const { declarationDom } = useDeclaration({
    clickText: 'Declaration Terms',
    modal: (
      <TermModal title="Declaration Terms">
        <div>
          {online.map((declaration, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <div className={styles.listItem} key={index}>
              <Typography level="body2" className={styles.number}>
                {index + 1}.{' '}
              </Typography>
              <Typography level="body2">{declaration}</Typography>
            </div>
          ))}
        </div>
      </TermModal>
    ),
  });

  const review = {
    type: 'Step',
    name: 'review',
    label: formatMessage({ id: 'Review and Submit' }),
    editButtonLabel: 'Edit',
    elements: [
      {
        render: () => {
          return (
            <>
              <Typography level="h3b">
                {formatMessage({
                  id: 'Please take a moment to review what you’ve told us above.',
                })}
              </Typography>
              <Typography level="body1" className={styles.tips}>
                {formatMessage({
                  id: 'Once you are happy with the details and have checked the box below, please submit your activation request.',
                })}
              </Typography>
            </>
          );
        },
      },
      {
        type: 'Divider',
      },
      {
        render: () => declarationDom,
      },
    ],
  };

  return {
    review,
  };
};

export default useReview;
