import { useContext } from 'react';
import { PolicyList } from 'iglooform';
import { HealthOutlined } from 'iglooicon';
import moment from 'moment';
import { PageLoading, CardLayout, useRequest, CardPage } from 'igloo-pa-tools';

import { getAllPolicies } from '@/services/policy';
import { GlobalContext } from '@/utils/context';
import {
  getStatus,
  POLICY_PROTECTED_STATUS,
  POLICY_EXPIRED_STATUS,
} from '@/components/StatusBox';

import type { FC } from 'react';
import styles from './index.less';

type Props = {
  onGoToDetail?: (id: string) => void;
};

const timeFormat = 'MM / DD / YYYY';

const Policy: FC<Props> = ({ onGoToDetail }) => {
  const { data, loading } = useRequest({
    func: getAllPolicies,
    automatic: true,
    dealResponse: (policyListRes: any) => {
      const { validPolicies, closedPolicies } = policyListRes;
      const validPolicyList: any[] = [...validPolicies];
      const closedPolicyList: any[] = [...closedPolicies];
      return {
        validPolicyList,
        closedPolicyList,
      };
    },
    defaultData: {},
  });

  const STATUS = getStatus();

  const { openPreviewModal, openCoverageModal, openStartModal } =
    useContext(GlobalContext);

  const dealPolicyData = (policy: any[]) => {
    return policy.map((item) => {
      const {
        state,
        productName,
        policyId,
        loanTerm,
        startAt,
        endAt,
        cocDoc,
        coverage,
        insured: { fullName, birth, gender, mobile, email, address },
      } = item;

      const buttons: any[] = [];

      if (
        state === POLICY_PROTECTED_STATUS ||
        state === POLICY_EXPIRED_STATUS
      ) {
        buttons.push({
          label: 'Claim Now',
          onClick: () => {
            openStartModal(policyId);
          },
          type: 'primary',
        });
      }

      return {
        insurerLogo: <HealthOutlined />,
        insurerName: productName,
        statusProps: STATUS[state],
        buttons,
        mobileSubTitle: policyId,
        mobileTagOnClick: () => onGoToDetail(policyId),
        defaultItems: [
          {
            title: 'COC No.',
            info: {
              text: policyId,
              link: {
                linkText: 'View COC',
                onClick: () => {
                  openPreviewModal(cocDoc);
                },
              },
            },
          },
          {
            title: 'Category',
            info: {
              text: 'Health and Personal Accident',
              link: {
                linkText: <div>View Coverage</div>,
                onClick: () => {
                  openCoverageModal(coverage);
                },
              },
            },
          },
          {
            title: 'Loan Term',
            info: `${loanTerm} months`,
          },
          {
            title: 'Coverage Start Date',
            info: moment(startAt).format(timeFormat),
          },
        ],
        infoGroups: [
          {
            infoItems: [
              {
                title: 'Coverage End Date',
                info: moment(endAt).format(timeFormat),
              },
            ],
          },
          {
            sectionName: 'Insured’s Details',
            infoItems: [
              {
                title: 'Full Name',
                info: fullName,
              },
              {
                title: 'Gender',
                info: gender,
              },
              {
                title: 'Date of Birth',
                info: moment(birth).format('MM / DD / YYYY'),
              },
              {
                title: 'Mobile Number',
                info: mobile,
              },
              {
                title: 'Email',
                info: email,
              },
              {
                title: 'Address',
                info: address,
              },
            ],
          },
        ],
      };
    });
  };

  if (loading) {
    return <PageLoading />;
  }

  const { validPolicyList = [], closedPolicyList = [] } = data;

  return (
    <CardPage
      title="My Policy"
      subtitle="Welcome to your Igloo account. All your insurance policies are listed as below."
      className={styles.cardPage}
    >
      <CardLayout
        title="Valid Policies"
        emptyText="There is currently no valid policy."
      >
        {validPolicyList.length > 0 && (
          <PolicyList dataList={dealPolicyData(validPolicyList)} />
        )}
      </CardLayout>

      <CardLayout
        title="Closed Policies"
        emptyText="There is currently no closed policy."
      >
        {closedPolicyList.length > 0 && (
          <PolicyList dataList={dealPolicyData(closedPolicyList)} />
        )}
      </CardLayout>
    </CardPage>
  );
};

export default Policy;
