import { StatusBox } from 'iglooform';

export const QUOTE_PENDING_STATUS = 'ActivationPending';
export const QUOTE_EXPIRED_STATUS = 'QuoteExpired';

export const POLICY_ISSUED_STATUS = 'PolicyIssued';
export const POLICY_PROTECTED_STATUS = 'PolicyProtected';
export const POLICY_EXPIRED_STATUS = 'PolicyExpired';

export const CLAIM_PROCESS_STATUS = 'ClaimProcessing';
export const CLAIM_APPROVE_STATUS = 'ClaimApproved';
export const CLAIM_REJECT_STATUS = 'ClaimRejected';
export const CLAIM_CANCEL_STATUS = 'ClaimCancelled';

export const REIMBURSE_PROCESS_STATUS = 'ReimbursementProcessing';
export const REIMBURSED_STATUS = 'Reimbursed';

export const getStatus = (): ObjectAny => {
  return {
    [QUOTE_PENDING_STATUS]: {
      type: 'warning',
      children: 'ACTIVATION PENDING',
    },
    [QUOTE_EXPIRED_STATUS]: {
      type: 'faild',
      children: 'QUOTE EXPIRED',
    },
    [POLICY_ISSUED_STATUS]: {
      type: 'success',
      children: 'ISSUED',
    },
    [POLICY_PROTECTED_STATUS]: {
      type: 'success',
      children: 'PROTECTED',
    },
    [POLICY_EXPIRED_STATUS]: {
      type: 'faild',
      children: 'POLICY EXPIRED',
    },

    [CLAIM_PROCESS_STATUS]: {
      type: 'waiting',
      children: 'CLAIM PROCESSING',
    },
    [CLAIM_APPROVE_STATUS]: {
      type: 'success',
      children: 'CLAIM APPROVED',
    },
    [CLAIM_REJECT_STATUS]: {
      type: 'faild',
      children: 'CLAIM REJECTED',
    },
    [CLAIM_CANCEL_STATUS]: {
      type: 'faild',
      children: 'CLAIM CANCELLED',
    },

    [REIMBURSE_PROCESS_STATUS]: {
      type: 'waiting',
      children: 'REIMBURSEMENT PROCESSING',
    },
    [REIMBURSED_STATUS]: {
      type: 'faild',
      children: 'REIMBURSED',
    },
  };
};

export default ({ status }: { status: string }) => {
  const STATUS = getStatus();
  return (
    <StatusBox
      type={
        STATUS[status].type as
          | 'warning'
          | 'success'
          | 'faild'
          | 'error'
          | 'waiting'
      }
    >
      {STATUS[status].value || STATUS[status].children}
    </StatusBox>
  );
};
