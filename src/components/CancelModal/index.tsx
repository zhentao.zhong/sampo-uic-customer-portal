import type { FC } from 'react';
import { Typography, Button } from 'iglooform';
import { Modal } from 'igloo-pa-tools';
import styles from './index.less';

interface Props {
  visible: boolean;
  onOk: () => void;
  onCancel: () => void;
  loading?: boolean;
}

const CanCelModal: FC<Props> = ({
  visible,
  onOk,
  onCancel,
  loading = false,
}) => {
  return (
    <Modal
      title="Cancel Claim"
      visible={visible}
      onCancel={onCancel}
      footer={
        <>
          <Button onClick={onOk} className={styles.btn}>
            Yes, Cancel
          </Button>
          <Button type="primary" onClick={onCancel} className={styles.btn}>
            Do Not Cancel
          </Button>
        </>
      }
      wrapClassName={styles.modal}
      confirmLoading={loading}
    >
      <Typography level="body1">
        Are you sure you want to cancel this claim?
      </Typography>
    </Modal>
  );
};

export default CanCelModal;
