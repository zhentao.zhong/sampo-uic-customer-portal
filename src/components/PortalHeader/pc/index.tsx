import { Divider } from 'antd';
import { Typography } from 'iglooform';
import { ArrowDownOutlined, AccountFilled, ArrowUpOutlined } from 'iglooicon';
import { useFade, Header } from 'igloo-pa-tools';

import useDropMenu from '@/hooks/useDropMenu';
import useMenu from '@/hooks/useMenu';
import { CUSTOMER_PORTAL_MENUS } from '@/params';

import type { FC } from 'react';

import styles from './index.less';

interface DropDownMenuProps {
  toggle: boolean;
  userEmail: string | undefined;
  onLogout: () => void;
}

const DropDownMenu: FC<DropDownMenuProps> = ({
  toggle,
  userEmail,
  onLogout,
}) => {
  const { fadeStyle, animated } = useFade(toggle);

  return (
    <animated.div
      className={styles.dropDownMenu}
      id="dropMenu"
      style={fadeStyle}
    >
      <Typography level="body2" className={styles.infoItem}>
        Email: {userEmail}
      </Typography>
      <Divider />
      <Typography level="body1" className={styles.btnItem}>
        <div onClick={onLogout}>Logout</div>
      </Typography>
    </animated.div>
  );
};

export default () => {
  const { toggle, userInfo, onToggle, onLogout } = useDropMenu();

  const { onChange, activeMenu } = useMenu();

  return (
    <>
      <Header.Menu
        menus={CUSTOMER_PORTAL_MENUS}
        onChange={onChange}
        active={activeMenu}
      />
      <div className={styles.infoContent}>
        <AccountFilled className={styles.accountImg} />
        <Typography level="body1">{userInfo.userName}</Typography>
        {toggle ? (
          <ArrowUpOutlined className={styles.iconActive} onClick={onToggle} />
        ) : (
          <ArrowDownOutlined className={styles.icon} onClick={onToggle} />
        )}

        <DropDownMenu
          toggle={toggle}
          userEmail={userInfo.userEmail}
          onLogout={onLogout}
        />
      </div>
    </>
  );
};
