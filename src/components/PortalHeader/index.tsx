import { useContext } from 'react';
import { Header } from 'igloo-pa-tools';

import { PcContext } from '@/utils/context';

import HeaderPc from './pc';
import HeaderH5 from './h5';

export default () => {
  const pcMark = useContext(PcContext);
  return <Header>{pcMark ? <HeaderPc /> : <HeaderH5 />}</Header>;
};
