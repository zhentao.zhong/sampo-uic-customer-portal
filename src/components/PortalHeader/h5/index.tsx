import { Typography } from 'iglooform';
import { useFade } from 'igloo-pa-tools';
import { BurgerMenuOutlined, AccountFilled, CrossOutlined } from 'iglooicon';

import useDropMenu from '@/hooks/useDropMenu';

import styles from './index.less';

interface DropDownMenuProps {
  toggle: boolean;
  userInfo: {
    userName?: string;
    userEmail?: string;
  };
  onLogout: () => void;
}

const DropDownMenu = ({ toggle, userInfo, onLogout }: DropDownMenuProps) => {
  const { fadeStyle, animated } = useFade(toggle);

  return (
    <animated.div
      className={styles.dropDownContent}
      id="dropMenu"
      style={fadeStyle}
    >
      <div className={styles.infoBox}>
        <AccountFilled className={styles.accountImg} />
        <Typography level="body1">{userInfo.userName}</Typography>
        <br />
        <Typography level="body3">Email: {userInfo.userEmail}</Typography>
      </div>
      <Typography level="body1" className={styles.btnItem}>
        <div onClick={onLogout}>Logout</div>
      </Typography>
    </animated.div>
  );
};

export default () => {
  const { toggle, onToggle, userInfo, onLogout } = useDropMenu();

  return (
    <>
      <div className={styles.menuContent}>
        {toggle ? (
          <CrossOutlined className={styles.icon} onClick={onToggle} />
        ) : (
          <BurgerMenuOutlined className={styles.icon} onClick={onToggle} />
        )}
        <DropDownMenu toggle={toggle} userInfo={userInfo} onLogout={onLogout} />
      </div>
    </>
  );
};
