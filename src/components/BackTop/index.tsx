import { BackTop } from 'iglooform';
import { PAGE_ROOT } from '@/params';

export default () => {
  return (
    <BackTop
      style={{ position: 'fixed', right: '40px', bottom: '20px' }}
      visibilityHeight={60}
      target={() => document.getElementById(PAGE_ROOT) || window}
    />
  );
};
