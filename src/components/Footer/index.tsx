import { Row, Col } from 'antd';
import { useContext } from 'react';
import {
  PhoneOutlined,
  EmailOutlined,
  TimeOutlined,
  LocationOutlined,
} from 'iglooicon';

import { PcContext } from '@/utils/context';

import { IconSection, CopyRight } from './components';

import styles from './index.less';

const Terms = () => {
  return (
    <CopyRight>
      <span
        className={styles.link}
        onClick={() =>
          window.open(
            'https://contents.iglooinsure.com/privacypolicy',
            '_blank',
          )
        }
      >
        Privacy Policy
      </span>{' '}
      |{' '}
      <span
        className={styles.link}
        onClick={() =>
          window.open(
            'https://contents.iglooinsure.com/termsofservice',
            '_blank',
          )
        }
      >
        Terms of Service
      </span>
    </CopyRight>
  );
};

const Footer = () => {
  const pcMark = useContext(PcContext);
  if (pcMark) {
    return (
      <footer className={styles.container}>
        <div className={styles.pcContainer}>
          <div className={styles.contact}>
            <IconSection icon={<PhoneOutlined />}>(02) 839 67450</IconSection>

            <IconSection icon={<EmailOutlined />} justifyContent="center">
              cs.homecredit.ph@iglooinsure.com
            </IconSection>

            <IconSection icon={<TimeOutlined />} justifyContent="end">
              <div style={{ lineHeight: '18px' }}>10:00 AM - 5:00 PM</div>
              <div style={{ lineHeight: '18px' }}>
                Monday - Friday (except Public Holiday)
              </div>
            </IconSection>
          </div>
          <div className={styles.copyRight}>
            <CopyRight>Underwritten by Mercantile Insurance</CopyRight>
            <CopyRight>© 2021 Powered by Igloo. All Rights Reserved.</CopyRight>
            <Terms />
          </div>
        </div>
      </footer>
    );
  }

  return (
    <footer className={styles.container}>
      <div className={styles.mobileContainer}>
        <div className={styles.contact}>
          <Row className={styles.antdRow}>
            <Col span="24">
              <IconSection icon={<PhoneOutlined />}>(02) 839 67450</IconSection>
            </Col>
            <Col span="24">
              <IconSection icon={<EmailOutlined />}>
                cs.homecredit.ph@iglooinsure.com
              </IconSection>
            </Col>
            <Col span="24">
              <IconSection icon={<TimeOutlined />}>
                <div style={{ lineHeight: '18px' }}>09:00 AM - 06:00 PM</div>
              </IconSection>
            </Col>
            <Col span="24">
              <IconSection icon={<LocationOutlined />}>
                No 20 Nguyễn Thị Minh Khai street, Da Kao Ward, District 1, Ho
                Chi Minh City, Vietnam
              </IconSection>
            </Col>
          </Row>
        </div>
        <div className={styles.copyRight}>
          <CopyRight>© 2021 Powered by Igloo.</CopyRight>
          <CopyRight>All Rights Reserved.</CopyRight>
          <div style={{ marginTop: '8px' }}>
            <Terms />
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
