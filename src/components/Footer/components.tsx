import React, { useContext } from 'react';
import { Typography } from 'iglooform';
import type { FC } from 'react';

import { PcContext } from '@/utils/context';

import styles from './index.less';

type Props = {
  icon: JSX.Element;
  justifyContent?: any;
};

export const IconSection: FC<Props> = ({
  icon,
  children,
  justifyContent = 'start',
}) => {
  const pcMark = useContext(PcContext);

  return (
    <section className={styles.section} style={{ justifyContent }}>
      {React.cloneElement(icon, { className: styles.icon })}
      <Typography
        level={pcMark ? 'body2' : 'body3'}
        className={styles.rightText}
      >
        {children}
      </Typography>
    </section>
  );
};

export const CopyRight: FC = ({ children }) => {
  return (
    <Typography level="body3" style={{ display: 'block' }}>
      {children}
    </Typography>
  );
};
