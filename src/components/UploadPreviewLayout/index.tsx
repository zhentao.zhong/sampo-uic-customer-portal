import { UploadPreview } from 'iglooform';

function UploadPreviewLayout({ files }: { files: string[] }) {
  return (
    <UploadPreview
      files={files}
      label="file"
      description={`${files.length} file${
        files.length > 1 ? 's' : ''
      } attached.`}
    />
  );
}

export default UploadPreviewLayout;
