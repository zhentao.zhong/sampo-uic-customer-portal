import { useContext } from 'react';
import { Typography } from 'iglooform';

import { PcContext } from '@/utils/context';

import type { FC, ReactNode } from 'react';
import styles from './index.less';

interface layoutProps {
  icon?: ReactNode | ReactNode[];
  title: ReactNode | ReactNode[];
}

const IconInfo: FC<layoutProps> = ({ icon, title, children }) => {
  const pcMark = useContext(PcContext);

  return (
    <div className={styles.content}>
      <div className={styles.icon}>{icon}</div>
      <div className={styles.title}>
        <Typography level={pcMark ? 'h3a' : 'h5'}>{title}</Typography>
      </div>
      <Typography className={styles.body} level={pcMark ? 'body1' : 'body2'}>
        {children}
      </Typography>
    </div>
  );
};

export default IconInfo;
