import { TabsHeaderMobile } from 'igloo-pa-tools';

import useMenu from '@/hooks/useMenu';
import { CUSTOMER_PORTAL_MENUS } from '@/params';

type TabsProps = {
  active: string;
};

export default ({ active }: TabsProps) => {
  const { onChange } = useMenu();

  return (
    <TabsHeaderMobile
      menus={CUSTOMER_PORTAL_MENUS}
      onChange={onChange}
      active={active}
    />
  );
};
