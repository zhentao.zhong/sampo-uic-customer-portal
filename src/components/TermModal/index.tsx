import { useState, useRef, useEffect } from 'react';
import { Modal, Button } from 'iglooform';
import { throttle } from 'lodash';

import type { ModalProps } from 'antd';
import type { FC } from 'react';
import styles from './index.less';

const TermModal: FC<ModalProps> = ({
  children,
  visible,
  onOk,
  footer,
  ...otherProps
}) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [btnDisabled, setBtnDisabled] = useState(true);
  const bodyRef = useRef(null);

  useEffect(() => {
    if (bodyRef.current) {
      const {
        current: { scrollHeight, clientHeight },
      } = bodyRef;

      if (scrollHeight && clientHeight && scrollHeight === clientHeight) {
        setBtnDisabled(false);
      }
    }
  }, [visible]);

  const onScroll = (e: any) => {
    const { clientHeight, scrollHeight, scrollTop } = e.target;
    const isBottom = scrollTop + clientHeight >= scrollHeight;
    if (isBottom) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  };

  return (
    <Modal
      footer={
        footer ? (
          <Button
            // disabled={btnDisabled}
            disabled={false}
            onClick={onOk}
            className={styles.btn}
            type="primary"
          >
            I Agree
          </Button>
        ) : null
      }
      wrapClassName={`${styles.modal} ${footer ? '' : styles.wrapNoFooter}`}
      visible={visible}
      {...otherProps}
    >
      <div
        className={`${styles.body} ${footer ? '' : styles.bodyNoFooter}`}
        onScroll={throttle(onScroll, 300)}
        ref={bodyRef}
      >
        {children}
      </div>
    </Modal>
  );
};

export default TermModal;
