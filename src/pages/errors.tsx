import type { IRoute } from 'umi';

import lockSvg from '@/assets/lock.svg';

import styles from './errorPage.less';

const NotAuthorized: IRoute = () => {
  return (
    <div className={styles.container}>
      <img src={lockSvg} className={styles.icon} alt={'Invalid request'} />
      <div className={styles.title}>{'Invalid request'}</div>
      <div className={styles.subTitle}>
        If the problem continues, please contact the site owner{' '}
        <a className={styles.mailTo} href="mailto:cs.vn@igloo.insure">
          cs.vn@igloo.insure
        </a>
      </div>
    </div>
  );
};

export default NotAuthorized;
