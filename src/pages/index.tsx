import { Redirect } from 'umi';

export default () => {
  return <Redirect to="/h5/login" />;
};
