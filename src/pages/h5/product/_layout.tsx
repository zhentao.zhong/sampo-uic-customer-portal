import { history } from 'umi';
import { Header } from 'igloo-pa-tools';

import Footer from '@/components/Footer';
import { PAGE_ROOT } from '@/params';
import UicLogo from '@/assets/uicLogo.png';

import type { FC } from 'react';

import styles from './_layout.less';

const _layout: FC = (props) => {
  // purchase flow header don't fixed

  const { children } = props;

  if (history?.location?.pathname === '/h5/product') {
    return <>{children}</>;
  }

  return (
    <div id={PAGE_ROOT} className={styles.pageContent}>
      <Header>
        <div className={styles.withLogo}>
          <img src={UicLogo} />
        </div>
      </Header>
      <div className={styles.wrapper}>{children}</div>
      <Footer />
    </div>
  );
};

export default _layout;
