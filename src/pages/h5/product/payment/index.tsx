import { Typography, AlertMessage, Button, Modal } from 'iglooform';
import { useIntl } from 'umi';

import Steps from '@/components/Steps';

import type { FC } from 'react';
import styles from './index.less';

const PaymentPage: FC = () => {
  const { formatMessage } = useIntl();
  // const { tryAgain } = history;
  const tryAgain = 1;

  const onPay = () => {};

  const onCheck = () => {
    Modal.confirm({
      title: formatMessage({ id: 'Payment Confirmation' }),
      content: (
        <Typography level="body1">
          Have completed this payment process?
        </Typography>
      ),
      okText: formatMessage({ id: '' }),
    });
  };

  return (
    <div className={styles.pageContent}>
      <Steps current={1} />
      <Typography level="h3a" className={styles.title}>
        {formatMessage({ id: 'Almost There!' })}
      </Typography>
      <div className={styles.background}>
        <AlertMessage
          type="info"
          message={
            <Typography level="body2">
              {formatMessage({ id: 'You will make the transaction through' })}{' '}
              <Typography level="h5">
                {formatMessage({ id: 'Igloo Payment Service.' })}
              </Typography>
            </Typography>
          }
        />
        <div className={styles.amountCard}>
          <Typography level="h4">
            {formatMessage({ id: 'Total Amount Due:' })}
          </Typography>
          <Typography level="h2" className={styles.amount}>
            VND51,100
          </Typography>
        </div>
        <Button
          type="primary"
          className={styles.btn}
          onClick={tryAgain ? onCheck : onPay}
        >
          {!tryAgain
            ? formatMessage({ id: 'Check Out' })
            : formatMessage({ id: 'Try Again' })}
        </Button>
        {tryAgain && (
          <Typography level="caption2" className={styles.tips}>
            {formatMessage({
              id: 'Looks like the payment still pends, please try again.',
            })}
          </Typography>
        )}
      </div>
    </div>
  );
};

export default PaymentPage;
