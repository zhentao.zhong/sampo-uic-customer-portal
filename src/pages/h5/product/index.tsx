import { useIntl } from 'umi';
import { Button, Typography } from 'iglooform';
import { SmileBrand } from 'iglooicon';
import { useSpring, animated } from 'react-spring';

import logoSvg from '@/assets/igloo.svg';
import UicLogo from '@/assets/uicLogo.png';

import type { FC } from 'react';

import styles from './index.less';

const Welcome: FC = () => {
  const { formatMessage } = useIntl();

  const style1 = useSpring({
    from: { opacity: 0, marginTop: 120 },
    to: { opacity: 1, marginTop: 80 },
    config: { duration: 600 },
    delay: 300,
  });

  const style2 = useSpring({
    from: { opacity: 0, marginTop: 48 },
    to: { opacity: 1, marginTop: 8 },
    config: { duration: 600 },
    delay: 1000,
  });

  const style3 = useSpring({
    from: { opacity: 0, marginTop: 32 },
    to: { opacity: 1, marginTop: 32 },
    config: { duration: 800 },
    delay: 1700,
  });

  return (
    <div className={styles.content}>
      <div className={styles.body}>
        <div className={styles.logoBox}>
          <img src={logoSvg} className={styles.logo} />
          <div className={styles.line} />
          <img src={UicLogo} className={styles.logo} />
        </div>

        <animated.div className={styles.section1} style={style1}>
          <SmileBrand className={styles.smile} />
          <Typography level="h2">
            {formatMessage({
              id: `Welcome to the exclusive benefits program of Ahamove inc.`,
            })}
          </Typography>
        </animated.div>

        <animated.div className={styles.section2} style={style2}>
          <Typography level="h3b">
            {formatMessage({
              id: `This benefits program is only for the company's employees and their families.`,
            })}
          </Typography>
        </animated.div>

        <animated.div className={styles.section3} style={style3}>
          <Button className={styles.btn} type="primary">
            {formatMessage({ id: 'Start Now' })}
          </Button>
        </animated.div>
      </div>
    </div>
  );
};

export default Welcome;
