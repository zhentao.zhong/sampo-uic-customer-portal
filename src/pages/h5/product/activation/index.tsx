import Steps from '@/components/Steps';
import ActivationForm from '@/components/ActivationForm';

import type { FC } from 'react';
import styles from './index.less';

const Activation: FC = () => {
  return (
    <div className={styles.pageContext}>
      {/* <PageLoading /> */}

      <Steps current={1} />
      <ActivationForm />
    </div>
  );
};

export default Activation;
