import logoSvg from '@/assets/igloo.svg';
import LoginOpt from '@/components/LoginOpt';
import useLogin from '@/hooks/useLogin';

import styles from './index.less';

const Login = () => {
  const { onSubmit, email, loading, onSend } = useLogin();
  return (
    <div className={styles.pageContent}>
      <img src={logoSvg} className={styles.logo} />
      <div className={styles.pageLogin}>
        <LoginOpt
          onSubmit={onSubmit}
          email={email}
          loading={loading}
          onSend={onSend}
        />
      </div>
    </div>
  );
};

Login.wrappers = ['@/wrappers/adaption'];
export default Login;
