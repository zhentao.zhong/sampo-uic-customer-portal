import { history } from 'umi';

import Footer from '@/components/Footer';
import PortalHeader from '@/components/PortalHeader';
import { PAGE_ROOT } from '@/params';

import type { FC } from 'react';

import styles from './_layout.less';

const _layout: FC = (props) => {
  const { children } = props;

  if (history?.location?.pathname === '/h5/login') {
    return <>{children}</>;
  }

  return (
    <>
      <PortalHeader />
      <div className={styles.pageContent} id={PAGE_ROOT}>
        <div className={styles.wrapper}>{children}</div>
        <Footer />
      </div>
    </>
  );
};

export default _layout;
