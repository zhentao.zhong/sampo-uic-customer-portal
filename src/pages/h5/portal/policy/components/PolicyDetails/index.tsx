import { useContext } from 'react';
import moment from 'moment';
import { HealthOutlined } from 'iglooicon';
import { MobileInsurerDetail } from 'iglooform';
import { useRequest, BackMobile, PageLoading } from 'igloo-pa-tools';

import type { FC } from 'react';

import { GlobalContext } from '@/utils/context';
import useScrollToTop from '@/hooks/useScrollToTop';
import {
  getStatus,
  POLICY_PROTECTED_STATUS,
  POLICY_EXPIRED_STATUS,
} from '@/components/StatusBox';
import { getPolicyDetail } from '@/services/policy';
import BackTop from '@/components/BackTop';

import styles from './index.less';

const timeFormat = 'MM / DD / YYYY';

type Props = {
  policyId: string;
  onBack: () => void;
};

const PolicyDetails: FC<Props> = (props) => {
  const { policyId, onBack } = props;

  const { data, loading, error } = useRequest({
    func: getPolicyDetail,
    params: policyId,
    dealResponse: (res) => res.policyDetail,
    automatic: true,
  });

  const { openPreviewModal, openCoverageModal, openStartModal } =
    useContext(GlobalContext);

  useScrollToTop();

  if (error) {
    onBack();
  }

  if (loading || !data) {
    return <PageLoading />;
  }

  const STATUS = getStatus();

  const buttons: any[] = [];

  if (
    data.state === POLICY_PROTECTED_STATUS ||
    data.state === POLICY_EXPIRED_STATUS
  ) {
    buttons.push({
      label: 'Claim Now',
      onClick: () => openStartModal(policyId),
      type: 'primary',
    });
  }

  const { startAt, endAt, loanTerm } = data;

  const sectionArea1 = {
    infoItems: [
      {
        title: 'COC No.',
        info: {
          text: policyId,
          link: {
            linkText: 'View COC',
            onClick: () => openPreviewModal(data?.cocDoc),
          },
        },
      },
      {
        title: 'Category',
        info: {
          text: 'Health and Personal Accident',
          link: {
            linkText: <div>View Coverage</div>,
            onClick: () => openCoverageModal(data?.coverage),
          },
        },
      },
      {
        title: 'Loan Term',
        info: `${loanTerm} months`,
      },
      {
        title: 'Coverage Start Date',
        info: moment(startAt).format(timeFormat),
      },
      {
        title: 'Coverage End Date',
        info: moment(endAt).format(timeFormat),
      },
    ],
  };

  const { fullName, birth, gender, mobile, email, address } =
    data?.insured || {};

  const sectionArea2 = {
    sectionName: 'Insured’s Details',
    infoItems: [
      {
        title: 'Full Name',
        info: fullName,
      },
      {
        title: 'Gender',
        info: gender,
      },
      {
        title: 'Date of Birth',
        info: moment(birth).format('MM / DD / YYYY'),
      },
      {
        title: 'Mobile Number',
        info: mobile,
      },
      {
        title: 'Email',
        info: email,
      },
      {
        title: 'Address',
        info: address,
      },
    ],
  };

  return (
    <div className={styles.details}>
      <BackMobile onClick={onBack} />
      <MobileInsurerDetail
        insurerLogo={<HealthOutlined />}
        insurerName={data.productName}
        buttons={buttons}
        statusProps={STATUS[data.state]}
        infoGroups={[sectionArea1, sectionArea2]}
      />
      <BackTop />
    </div>
  );
};

export default PolicyDetails;
