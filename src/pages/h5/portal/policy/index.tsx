import { useState } from 'react';

import PolicyList from '@/components/Policy';
import TabsHeader from '@/components/TabsHeader';
import PolicyDetails from './components/PolicyDetails';

const Policy = () => {
  const [policyId, setPolicyId] = useState<string | undefined>(undefined);

  const onBack = () => {
    setPolicyId(undefined);
  };

  if (policyId) {
    return <PolicyDetails policyId={policyId} onBack={onBack} />;
  }

  return (
    <>
      <TabsHeader active="/policy" />
      <PolicyList
        onGoToDetail={(id: string) => {
          setPolicyId(id);
        }}
      />
    </>
  );
};

Policy.wrappers = ['@/wrappers/auth', '@/wrappers/adaption'];
export default Policy;
