import { useIntl } from 'umi';
import type { IRoute } from 'umi';

import notAuthorizedSvg from '@/assets/notAuthorized.svg';

import styles from './errorPage.less';

const NotAuthorized: IRoute = () => {
  const { formatMessage } = useIntl();

  return (
    <div className={styles.container}>
      <img
        className={styles.icon}
        alt="not authorized"
        src={notAuthorizedSvg}
      />
      <div className={styles.primary}>
        {formatMessage({ id: 'Something went wrong' })}
      </div>
    </div>
  );
};

export default NotAuthorized;
