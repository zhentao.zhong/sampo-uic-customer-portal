import { useIntl } from 'umi';
import type { IRoute } from 'umi';

import forbiddenSvg from '@/assets/forbidden.svg';

import styles from './errorPage.less';

const Forbidden: IRoute = () => {
  const { formatMessage } = useIntl();

  return (
    <div className={styles.container}>
      <img className={styles.icon} alt="forbidden" src={forbiddenSvg} />
      <div className={styles.primary}>
        {formatMessage({ id: 'Something went wrong' })}
      </div>
    </div>
  );
};

export default Forbidden;
