import type { FC } from 'react';
import { useIntl } from 'umi';
import type { IRoute } from 'umi';

import notFoundSvg from '@/assets/notFound.svg';

import styles from './errorPage.less';

export const NotFoundComponent: FC = () => {
  const { formatMessage } = useIntl();
  return (
    <div className={styles.container}>
      <img className={styles.icon} alt="not found" src={notFoundSvg} />
      <div className={styles.primary}>
        {formatMessage({ id: '404: Resource not found' })}
      </div>
    </div>
  );
};

const NotFound: IRoute = () => <NotFoundComponent />;

NotFound.path = undefined;
NotFound.exact = undefined;

export default NotFound;
