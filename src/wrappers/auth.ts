import { history } from 'umi';
import type { IRouteComponentProps } from 'umi';

import { getJWT } from '@/utils/jwt';
import { goUrl } from '@/utils/route';

export default ({ children }: IRouteComponentProps) => {
  const token = getJWT();

  if (!token) {
    const customerId = localStorage.getItem('customerId');
    if (!customerId) {
      history.push(`/401`);
    }
    goUrl(`/login?id=${customerId}`);
  }

  return children;
};
