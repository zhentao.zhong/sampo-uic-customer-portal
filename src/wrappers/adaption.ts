import { history } from 'umi';
import type { IRouteComponentProps } from 'umi';

import { checkPc } from 'igloo-pa-tools';

export default ({ children }: IRouteComponentProps) => {
  const { pathname, search } = window.location;
  const pcMark = checkPc();

  // if the platform is pc and the href contain h5 route, redirect to pc route
  if (pcMark && pathname && /h5/.test(pathname)) {
    // if the href contain h5/policy route, redirect to /pc/policy
    if (/h5\/policy/.test(pathname)) {
      history.replace('/pc/policy');
    } else {
      history.replace({ pathname: pathname.replace('h5', 'pc'), search });
    }
  }
  // if the platform is mobile and the href contain pc route, redirect to h5 route
  else if (!pcMark && pathname && /pc/.test(pathname)) {
    history.replace({ pathname: pathname.replace('pc', 'h5'), search });
  }

  return children;
};
