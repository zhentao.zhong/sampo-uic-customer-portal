import { request } from 'umi';

export function getAllQuotes(): Promise<{ quotes: any[] }> {
  return request('/customer/quotes', {
    method: 'GET',
  });
}

export function getAllPolicies(): Promise<{
  validPolicies: any[];
  closedPolicies: any[];
}> {
  return request('/customer/policies', {
    method: 'GET',
  });
}

export function getQuoteDetail(quoteId: string) {
  return request(`/customer/quotes/${quoteId}`, {
    method: 'GET',
  });
}

export function getPolicyDetail(policyId: string) {
  return request(`/customer/policies/${policyId}`, {
    method: 'GET',
  });
}

export function getPolicyId() {
  return request(`/customer/policyIds`, {
    method: 'GET',
  });
}

export function checkPolicyId(policyId: string) {
  return request(`/check/policy/${policyId}`, {
    method: 'GET',
  });
}
