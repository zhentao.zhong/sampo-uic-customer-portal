import { request } from 'umi';

export function createActivation(payload: any) {
  return request('/customer/claim/create', {
    method: 'POST',
    data: {
      ...payload,
    },
  });
}

export function createDeathActivation(payload: any) {
  return request('/death/claim/create', {
    method: 'POST',
    data: {
      ...payload,
    },
  });
}

export function prefill(policyId: string) {
  return request(`/prefill/${policyId}`, {
    method: 'GET',
  });
}
