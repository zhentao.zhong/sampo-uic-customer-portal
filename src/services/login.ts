import { request } from 'umi';

export function getEmail(id: string) {
  return request('/login', {
    method: 'GET',
    params: {
      id,
    },
  });
}

export function sendOtp(data: any) {
  return request('/login/otp-code', {
    prefix: LOGIN_URL,
    method: 'POST',
    data: {
      ...data,
    },
  });
}

export function otpCheck(data: any) {
  return request('/login/otp', {
    prefix: LOGIN_URL,
    method: 'POST',
    data: {
      ...data,
    },
  });
}
