import { request } from 'umi';

export const upload = (file: any) => {
  const data = new FormData();
  data.append('file', file);
  data.append('biz_key', PLATFORM);
  data.append('is_public', 'false');
  data.append('with_content', 'true');

  const res = request('/common_file/upload', {
    prefix: FILE_URL,
    method: 'POST',
    data,
  });
  return res;
};
