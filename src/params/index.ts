/* eslint-disable react-hooks/rules-of-hooks */
import { useIntl } from 'umi';

export interface MenuType {
  name: string;
  route: string;
}

export const CUSTOMER_PORTAL_MENUS: MenuType[] = [
  {
    name: 'My Policy',
    route: '/policy',
  },
  {
    name: 'Claim',
    route: '/claim',
  },
];

export const ACCEPT: string = 'application/pdf,image/jpeg,image/png,image/jpg';

export const PAGE_ROOT: string = `${PLATFORM}-pageContent`;

export const getRelationship = () => {
  const { formatMessage } = useIntl();
  return [
    {
      value: 'Self',
      label: formatMessage({ id: 'Self' }),
    },
    {
      value: 'Spouse',
      label: formatMessage({ id: 'Spouse' }),
    },
    {
      value: 'Father',
      label: formatMessage({ id: 'Father' }),
    },
    {
      value: 'Mother',
      label: formatMessage({ id: 'Mother' }),
    },
    {
      value: 'Child',
      label: formatMessage({ id: 'Child' }),
    },
    {
      value: 'Sister',
      label: formatMessage({ id: 'Sister' }),
    },
    {
      value: 'Brother',
      label: formatMessage({ id: 'Brother' }),
    },
    {
      value: 'Aunt',
      label: formatMessage({ id: 'Aunt' }),
    },
    {
      value: 'Uncle',
      label: formatMessage({ id: 'Uncle' }),
    },
    {
      value: 'Paternal grandfather',
      label: formatMessage({ id: 'Paternal grandfather' }),
    },
    {
      value: 'Paternal grandmother',
      label: formatMessage({ id: 'Paternal grandmother' }),
    },
    {
      value: 'Maternal grandfather',
      label: formatMessage({ id: 'Maternal grandfather' }),
    },
    {
      value: 'Maternal grandmother',
      label: formatMessage({ id: 'Maternal grandmother' }),
    },
  ];
};

export const getGender = () => {
  const { formatMessage } = useIntl();
  return [
    {
      value: 'Female',
      label: formatMessage({ id: 'Female' }),
    },
    {
      value: 'Male',
      label: formatMessage({ id: 'Male' }),
    },
  ];
};

export const getIdentificationType = () => {
  const { formatMessage } = useIntl();
  return [
    {
      value: 'ID card',
      label: formatMessage({ id: 'ID card' }),
    },
    {
      value: 'CCCD',
      label: formatMessage({ id: 'CCCD' }),
    },
    {
      value: 'Passport',
      label: formatMessage({ id: 'Passport' }),
    },
  ];
};
