import React from 'react';

export const PcContext = React.createContext(false);

export const GlobalContext = React.createContext<any>({});
