import { history } from 'umi';
import { checkPc } from 'igloo-pa-tools';

export const goUrl = (option: any) => {
  if (typeof option === 'string') {
    history.push(`/${checkPc() ? 'pc' : 'h5'}${option}`);
  } else {
    history.push({
      ...option,
      pathname: `/${checkPc() ? 'pc' : 'h5'}${option.pathname}`,
    });
  }
};
