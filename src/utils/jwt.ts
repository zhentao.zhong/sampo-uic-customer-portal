type JWT = unknown;

const key = `${PLATFORM}_${ENV}_jwt`;

const getJWT = (): string | undefined => {
  const jwtToken = localStorage.getItem(key);
  if (jwtToken) return jwtToken;
  return undefined;
};

const setJWT = (token: string | undefined): string | undefined => {
  if (token) localStorage.setItem(key, token);
  return token;
};

const removeJWT = (): void => {
  localStorage.removeItem(key);
};

const parseJWT = (token: string): JWT | null => {
  try {
    if (token) {
      const base64Url: string = token.split('.')[1];
      const base64: string = base64Url.replace('-', '+').replace('_', '/');
      return JSON.parse(window.atob(base64));
    }
    return null;
  } catch (error) {
    return null;
  }
};

const setLocalStorage = (data: Record<string, string>) => {
  for (const [name, value] of Object.entries(data)) {
    localStorage.setItem(`${PLATFORM}_${ENV}_${name}`, value);
  }
};

const getLocalStorage = (name: string): undefined | string => {
  const value = localStorage.getItem(`${PLATFORM}_${ENV}_${name}`);
  if (value) return value;
  return undefined;
};

export {
  getJWT,
  setJWT,
  removeJWT,
  parseJWT,
  setLocalStorage,
  getLocalStorage,
};
