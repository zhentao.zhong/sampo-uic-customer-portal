export function ifObjectEveryValueIsNullString(obj: ObjectAny) {
  return Object.values(obj).every((value) => value === '');
}

export const formatAmount = (money: string): string => {
  return `₱${Number(money).toLocaleString('PHP', {
    minimumFractionDigits: 2,
  })}`;
};

export function mapValueToLabelText(
  opts: { label: any; value: any }[],
  val: any,
) {
  return opts.find((item) => item.value === val)?.label;
}
