import { useEffect } from 'react';
import { checkPc } from 'igloo-pa-tools';

import { PcContext, GlobalContext } from '@/utils/context';
import { PAGE_ROOT } from '@/params';

import type { FC } from 'react';
import type { Location } from 'umi';

interface layoutProps {
  location: Location;
}

const Layout: FC<layoutProps> = ({ children, location }) => {
  const pcMark = checkPc();

  useEffect(() => {
    document.getElementById(PAGE_ROOT)?.scrollTo(0, 0);
  }, [location.pathname]);

  return (
    <PcContext.Provider value={pcMark}>
      <GlobalContext.Provider value={{}}>{children}</GlobalContext.Provider>
    </PcContext.Provider>
  );
};

export default Layout;
