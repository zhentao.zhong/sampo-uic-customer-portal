const fs = require('fs');
const path = require('path');

function deleteFolderRecursive() {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach((file) => {
      const curPath = `${path}/${file}`;
      if (fs.statSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath);
      } else {
        // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

export default (api) => {
  api.onStart(async () => {
    deleteFolderRecursive(path.join(__dirname, '..', 'build'));
  });
};
