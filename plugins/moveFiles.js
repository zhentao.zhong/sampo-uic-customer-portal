const fs = require('fs');
const path = require('path');

const Platform = 'HomeCredit';

export default (api) => {
  api.onBuildComplete(async () => {
    const sourcePath = path.join(
      __dirname,
      '..',
      'build',
      Platform,
      'index.html',
    );
    const destPath = path.join(__dirname, '..', 'build', 'index.html');

    fs.rename(sourcePath, destPath, (err) => {
      if (err) throw err;
      fs.stat(destPath, (errs) => {
        if (errs) throw errs;
      });
    });
  });
};
