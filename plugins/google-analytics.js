const PROD_KEY = 'G-D536WC78CL';
const DEV_KEY = 'G-L1BDCT2JYT';

export default function GA(api) {
  const code = process.env.UMI_ENV === 'prod' ? PROD_KEY : DEV_KEY;

  const gaTpl = function createTpl() {
    return `
    (function(){
      var script = document.createElement('script');
      script.src = 'https://www.googletagmanager.com/gtag/js?id=${code}';
      script.async = true;
      script.onload = function() {
        window.dataLayer = window.dataLayer || [];
        window.gtag = function() {
          window.dataLayer.push(arguments);
        };
        window.gtag('js', new Date());
        window.gtag('config', '${code}');
      };
      document.getElementsByTagName('head')[0].appendChild(script);
    })();
  `;
  };

  api.addHTMLScripts(() => {
    return [
      {
        content: gaTpl(),
      },
    ];
  });
}
