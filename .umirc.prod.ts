import { defineConfig } from 'umi';

const FILE_URL = 'https://api.iglooinsure.com/v1/branch/fileapi';
const BASE_URL = 'https://api.iglooinsure.com/v1/home-credit';
const LOGIN_URL = 'https://api.iglooinsure.com/v1/customer';

export default defineConfig({
  define: {
    ENV: 'prod',
    FILE_URL,
    BASE_URL,
    LOGIN_URL,
  },
});
